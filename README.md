**Docker RabbitMQ Example Publish/Subscribe**

Se llevara a cabo el patrón conocido como "Publish/Subscribe", que envia un mensaje a múltiples consumidores.

Para ilustrar el patrón, vamos a construir un sistema de registro simple. Constará de dos programas: el primero emitirá mensajes de registro (`emit_log.py`) y el segundo los recibirá e imprimirá (`receive_logs.py`).

En nuestro sistema de registro, cada copia en ejecución del programa receptor recibirá los mensajes. De esa manera podremos ejecutar un receptor y dirigir los registros al disco; y al mismo tiempo podremos ejecutar otro receptor y ver los registros en la pantalla.

Básicamente, los mensajes de registro publicados se transmitirán a todos los receptores.

**Pre-requisitos**

- Docker Desktop

**Iniciando contenedores**

Para ejecutar el programa, debemos crear los contenedores, mediante la instrucción:

`docker-compose build`

Lo siguiente será iniciar los contenedores:

`docker-compose up`

**Emit Logs**

Para emitir mensajes, usaremos el siguiente comando:

`docker exec emit-logs python emit_log.py "ingrese algun mensaje"`

De esta forma podremos enviar mensajes, si no especificamos ninguno se envia por defecto "info: Hello World!"

**Receive Logs**

Para recibir los mensajes, usaremos el siguiente comando:

`docker exec receive-logs python receive_logs.py`

Este programa nos permitirá recibir los mensajes publicados y los mostrará por pantalla. Si queremos salir de la aplicación, ingresamos "CTRL-C".

Podemos abrir varios progamas en simultáneo, y todas recibiran los mensajes al mismo tiempo. 

